microchat
===

An experimental minimalistic ephemeral web-based chat. 

It is meant as a very quick way to exchange real-time messages with
somewhat-trusted parties, with a few peculiar characteristics:

* no custom application required, it works on desktop and mobile with
  just a web browser;
* no user registration process, no connections to external identities
  (emails) or any other form of long-term user identity;
* based on a *rendez-vous* workflow, participants agree on a "secret"
  beforehand, which in this case is the room name;
* no logs, messages are not persisted in any form except in the
  browser window of the participants: when it is closed, they're gone
  -- no room history is kept for new participants.

There is no access control on rooms: if you know a room name, you can
join it. This is partially mitigated by making room membership
prominently visible.

The chat implements end-to-end encryption, primarily as a way to make
it safe against corporate proxies and such, and to avoid liabilities
for instance operators: participants still need to trust the instance
operator, as it may be serving a back-doored version of the
application.

From a forensic perspective, all it requires from the participant is
the usage of a browser "incognito" window, to avoid leaving the
instance name in the browser history.
