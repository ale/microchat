var webpack = require('webpack');
var PROD = JSON.parse(process.env.PROD || '0');
var MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  //context: __dirname + '/microchat/static-src',
  context: __dirname,
  entry: {
    microchat: ['./app.js', './app.css'],
  },
  output: {
    publicPath: '/',
    path: __dirname + '/public',
    filename: PROD ? '[name]-[hash].min.js' : '[name].js',
  },
  plugins: [
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
    }),
    new MiniCssExtractPlugin(),
  ],
  resolve: {
      fallback: {
          'buffer': require.resolve('buffer/'),
          'crypto': require.resolve('crypto-browserify'),
          'path': require.resolve('path-browserify'),
          'stream': require.resolve('stream-browserify'),
      }
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
        //use: ['file-loader', 'extract-loader', 'css-loader'],
      },
      {
        test: /\.(png|gif|jpe?g|svg|eot|otf|ttf|woff)$/,
        use: ['url-loader?limit=8192'],
      },
    ],
  },
};


