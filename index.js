const server = require('server');
const { get, socket } = server.router;
const { render } = server.reply;

// Send a message to everyone.
const sendMessage = ctx => {
    var user = ctx.socket.handshake.query.user;
    var room = ctx.socket.handshake.query.room;
    var data = {
        encryptedMessages: ctx.data.encryptedMessages,
        user: user,
    };
    ctx.io.to(room).emit('message', data);
};

// A new user connects to a room. Validate user / room names to match
// what we are doing in app.js.
const joinRoom = ctx => {
    var user = ctx.socket.handshake.query.user;
    var room = ctx.socket.handshake.query.room;
    var userRx = /^\w{2,64}$/;
    var roomRx = /^[-_.\$\/@%a-zA-Z0-9]{3,64}$/;
    if (user.search(userRx) < 0 || room.search(roomRx) < 0) {
        throw "invalid user / room";
    }
    console.log('join id=' + ctx.socket.id + ', user=' + user + ', room=' + room);
    ctx.io.to(room).emit('user_join', {user: user});
    ctx.socket.join(room);
};

const leaveRoom = ctx => {
    var user = ctx.socket.handshake.query.user;
    var room = ctx.socket.handshake.query.room;
    console.log('leave id=' + ctx.socket.id + ', user=' + user + ', room=' + room);
    ctx.socket.leave(room);
    ctx.io.to(room).emit('user_leave', {user: user});
};

const updateKey = ctx => {
    var user = ctx.socket.handshake.query.user;
    var room = ctx.socket.handshake.query.room;
    console.log('received key from ' + user + ' for room ' + room);
    ctx.io.to(room).emit('update_key', {
        user: user, key: ctx.data.key, reply: ctx.data.reply});
};

server([
    get('/', ctx => render('index.html')),
    socket('connect', joinRoom),
    socket('disconnect', leaveRoom),
    socket('message', sendMessage),
    socket('update_key', updateKey),
]);
