FROM node:current-buster

ADD . /src
WORKDIR /src
RUN npm install && ./node_modules/.bin/webpack

CMD ["node", "."]
