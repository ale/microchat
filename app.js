import io from 'socket.io-client';

//import sodium from 'libsodium-wrappers';
const _sodium = require('libsodium-wrappers');
var sodium;

const concatTypedArray = require("concat-typed-array");

class EncryptionKey {
    constructor(data) {
        this._data = data;
    }

    publicKey() {
        return sodium.to_hex(this._data.publicKey);
    }

    encrypt(rcptKey, message) {
        let nonce = sodium.randombytes_buf(sodium.crypto_box_NONCEBYTES);
        return sodium.to_base64(
            concatTypedArray(Uint8Array, nonce, sodium.crypto_box_easy(
                message, nonce, rcptKey._data.publicKey, this._data.privateKey)));
    }
 
    decrypt(senderKey, encData) {
        let nonce_and_ciphertext = sodium.from_base64(encData);
        if (nonce_and_ciphertext.length < sodium.crypto_box_NONCEBYTES) {
            throw "Short message";
        }
        let nonce = nonce_and_ciphertext.slice(0, sodium.crypto_box_NONCEBYTES),
            ciphertext = nonce_and_ciphertext.slice(sodium.crypto_box_NONCEBYTES);
        return sodium.to_string(sodium.crypto_box_open_easy(
            ciphertext, nonce, senderKey._data.publicKey, this._data.privateKey));
    }

    static generate() {
        let data = sodium.crypto_box_keypair();
        return new EncryptionKey(data);
    }

    static load(pkey) {
        return new EncryptionKey({publicKey: sodium.from_hex(pkey)});
    }
};

$(function() {
    var user, room, key;
    var userKeys = new Map();

    var userRx = /^\w{2,64}$/;
    while (!user) {
        // Ask for the username if there is none set already
        user = prompt('Choose a username:');
        if (!user) {
            alert('Username cannot be empty');
        } else if (user.search(userRx) < 0) {
            alert('Username must be a short alphanumeric string');
            user = null;
        }
    }

    var roomRx = /^[-_.\$\/@%a-zA-Z0-9]{3,64}$/;
    while (!room) {
        room = prompt('Choose a room:');
        if (!room) {
            alert('Room cannot be empty');
        } else if (room.search(roomRx) < 0) {
            alert('Room name must be a short alphanumeric string');
            room = null;
        }
    }

    // Set the room title.
    $('.title').text('#' + room);

    var updateUserList = function() {
        var s = 'Users: ';
        var i = 0;
        for (let username of userKeys.keys()) {
            if (i > 0) {
                s += ', ';
            }
            i++;
            s += username;
        }
        $('.userlist').text(s);
    };

    // Initialize socket.io.
    var socket = io({
        query: {
            user: user,
            room: room
        }
    });

    (async() => {
        await _sodium.ready;
        sodium = _sodium;

        // Generate the key and advertise it when done.
        key = EncryptionKey.generate();
        userKeys.set(user, key);
        console.log('generated key: ' + key.publicKey())

        // Advertise our key to current members.
        socket.emit('update_key', {key: key.publicKey(), reply: false});
    })();

    // When we see a user advertising their key, reply with ours.
    socket.on('update_key', function (data) {
        var newKey = EncryptionKey.load(data.key);
        var isMe = data.user == user;
        if (isMe) {
            if (newKey.publicKey() != key.publicKey()) {
                // Spot duplicate usernames.
                console.log('someone else advertised my username with key ' + newKey.publicKey());
                var el = $('<p><i>someone else has logged in with the same username as you!</i></p>');
                $('.chat').append(el);
            }
        } else {
            // Add the key, and the user to the userlist.
            userKeys.set(data.user, newKey);
            updateUserList();
            console.log('received key for user ' + data.user + ': ' + newKey.publicKey());
            if (!data.reply) {
                // Send our key as a reply.
                socket.emit('update_key', {key: key.publicKey(), reply: true});
            }
        }
    });

    // When we receive a message
    // it will be like { user: 'username', message: 'text' }
    socket.on('message', function (data) {
        var message = '';

        // Try to decrypt one of the encrypted messages with our key;
        var senderKey = userKeys.get(data.user);
        for (var i = 0; i < data.encryptedMessages.length; i++) {
            try {
                var result = key.decrypt(senderKey, data.encryptedMessages[i]);
                if (result) {
                    console.log('successfully decrypted message');
                    message = result;
                    break;
                }
            } catch {};
        }

        if (message == '') {
            console.log('failed to decrypt message');
        } else {
            var el = $('<p><strong class="user"></strong>: <span class="msg"></span></p>');
            $('.user', el).text(data.user);
            $('.msg', el).text(message);
            $('.chat').append(el);

            $('.chat').scrollTop($('.chat').height());
        }
    });

    socket.on('user_join', function (data) {
        var el = $('<p><i><span class="user"></span> has joined</i></p>');
        $('.user', el).text(data.user);
        $('.chat').append(el);
    });

    socket.on('user_leave', function (data) {
        var el = $('<p><i><span class="user"></span> has left</i></p>');
        $('.user', el).text(data.user);
        $('.chat').append(el);

        userKeys.delete(data.user);
        updateUserList();
    });

    // Reload the page to leave the room.
    $('.leave').click(function() {
        window.location.reload();
        return false;
    });
    
    // When the form is submitted
    $('form').submit(function (e) {
        // Avoid submitting it through HTTP
        e.preventDefault();

        // Retrieve the message from the user
        var message = $(e.target).find('input').val();
        if (message.length) {
            // Send the message to the server
            var data = {
                //message: message,
                message: '*encrypted*',
                encryptedMessages: [],
            };
            userKeys.forEach((userkey, username, map) => {
                var enc = key.encrypt(userkey, message);
                console.log('encrypted message for ' + username);
                data.encryptedMessages.push(enc);
            });
            socket.emit('message', data);
        }

        // Clear the input and focus it for a new message
        e.target.reset();
        $(e.target).find('input').focus();
    });

});

